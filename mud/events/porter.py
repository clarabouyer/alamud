from .event import Event2

class PorterEvent(Event2):
	NAME = "porter"
	def perform(self):
		if not self.object.has_prop("portable"):
			self.fail()
			return self.inform("porter.failed")
		self.inform("porter")