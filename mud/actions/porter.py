from .action import Action2
from mud.events import PorterEvent

class PorterAction(Action2):
	EVENT = PorterEvent
	ACTION = "porter"
	RESOLVE_OBJECT = "resolve_for_use"